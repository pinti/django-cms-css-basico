from django.shortcuts import render
from django.http import HttpResponse, Http404
from .models import Contenido, Comentario
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.utils import timezone

@csrf_exempt
def get_content(request,llave):
	if request.method == "PUT":
		valor = request.body.decode("utf-8")

	if request.method == "POST":
		action = request.POST["action"]
		if action == "Enviar Contenido":
			valor = request.POST['valor']
			try:
				content = Contenido.objects.get(clave=llave)
				content.valor = valor
				content.save()
			except Contenido.DoesNotExist:
				content = Contenido(clave=llave, valor=valor)
				content.save()

		elif action == "Enviar Comentario":
			contenido = Contenido.objects.get(clave=llave)
			titulo = request.POST["titulo"]
			cuerpo = request.POST["cuerpo"]
			fecha = timezone.now()

			q = Comentario(contenido= contenido, titulo = titulo, cuerpo = cuerpo, fecha = fecha)
			q.save()

		
	try:
		content = Contenido.objects.get(clave=llave)
		context = {"content": content}
	except Contenido.DoesNotExist:
		return render(request,"cms/new.html",{})

	return render(request,"cms/content.html",context)
	
def index(request):
	
	content_list = Contenido.objects.all()
	template = loader.get_template("cms/index.html")
	context = {
		"content_list" : content_list
	}

	return HttpResponse(template.render(context,request))
	
def loggedIn(request):
	if request.user.is_authenticated:
		respuesta = "Logged in as " + request.user.username
	else:
		respuesta = "No estás autenticado. <a href =/login>Autentícate</a>"
	return HttpResponse(respuesta)
	
def loggedout_view(request):
	logout(request)
	return redirect("/cms/")

def imagen(request):
	template = loader.get_template("cms/plantilla.html")
	context = {}

	return HttpResponse(template.render(context,request))

def estilo(request):
    return render(request, "cms/main.css" , content_type="text/css")