from django.urls import path
from . import views

urlpatterns = [
	path("",views.index),
	path("loggedIn",views.loggedIn),
	path("loggedout",views.loggedout_view),
	path("imagen",views.imagen),
	path("main.css",views.estilo),
    path("<str:llave>",views.get_content)

]
